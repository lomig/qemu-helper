# Qemu helper

Basic script to facilitate the use of qemu from the command line

# Virtual machine creation

Done with `qemu new`

```
Arguments :
    -m|--machine : Name of the virtual machine
    -i|--iso : Path of the iso file
    -s|--size : Size of the virtual disk, 50G by default (Optional)
    --uefi : Enable UEFI installation
```

## Examples

`qemu new -m Ubuntu -i ./ubuntu.iso`

`qemu new -m Deepin -i ./deepin.iso -s 64G`

When you create a virtual machine, qemu creates these folder and files :

```
Ubuntu/
    install.qcow2
    .snapshots/
        regular.qcow2
```

The install.qcow2 is the disk created just after the install of the distribution with the livecd
It clones it in `.snapshots/regular.qcow2` and reboots on. So after the installation, don't reboot, just close the first virtual machine.

It also creates executable files `q-deepin-regular` and `q-deepin-regular-vnc`
The content is about that


`$HOME/.local/bin/qemu launch Deepin regular`

and

`$HOME/.local/bin/qemu launch-vnc Deepin regular`

# Snapshot creation

Done with `qemu new snapshot`

```
Arguments :
    -n|--name : Name of the snapshot
    -m|--machine : Name of the machine to base on
```

## Example

Imagine you want a snapshot of Ubuntu to get an official gnome-shell appearance :

`qemu new snapshot -m Ubuntu -n gnome-shell`

You will get :

```
Ubuntu/
    install.qcow2
    .snapshots/
        gnome-shell.qcow2
        regular.qcow2
```

and the executable `q-ubuntu-gnome-shell`

# List the system and their snapshots :

```
qemu list

* Deepin
* Ubuntu
   - gnome-shell
```

# Remove system

Done with `qemu remove`

```
Arguments :
    -m|--machine : Name of the virtual machine
```

## Example

`qemu remove -m Ubuntu` : removes all disk and snapshots of Ubuntu

# Remove snapshots

Done with `qemu remove snapshots`

```
Arguments :
    -n|--name : Name of the snapshot
    -m|--machine : Name of the virtual machine
```

## Example

`qemu remove snapshot -m Ubuntu -n gnome-shell` : removes the snapshot gnome-shell for Ubuntu
